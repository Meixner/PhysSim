from PyQt5.QtWidgets import *
from gui.EnvironmentSettingsWidget import EnvironmentSettingsWidget
from physEngine.python.BulletPhysSim import BulletPhysSim
# add additional python physics engine imports here
import os
from importlib import util
cppFound = os.path.isdir("physEngine/cpp/build") and util.find_spec('physEngine.cpp.build.cppProject') is not None
if (cppFound): import physEngine.cpp.build.cppProject
import sys

physEngines = []
physEngines.append(('PyBullet', BulletPhysSim()))
# add additional python physics engines here
if (cppFound):
    physEngines.append(('CppTest', physEngine.cpp.build.cppProject.TestEngine()))
    # add additional c++ physics engines here

app = QApplication(sys.argv)
window = EnvironmentSettingsWidget(physEngines)
sys.exit(app.exec_())