from definitions import EXPERIMENT_FILES, name
import math

if __name__ == '__main__':
    for filePath in EXPERIMENT_FILES:
        file = open(str(filePath), 'r')
        fileName = name(filePath)
        print(fileName)
        index = 0
        height = 1.0
        bounce_heights = []

        for line in file:
            if index == 0:
                height = float(line)
            if index > 1:
                bounce_heights.append(float(line))
            index += 1

        restitutionValues = []
        restitutionMean = 0
        heightMean = 0
        for bounce_height in bounce_heights:
            heightMean += bounce_height
            restitutionValue = math.sqrt(bounce_height / height)
            restitutionValues.append(restitutionValue)
            restitutionMean += restitutionValue

        heightMean /= len(restitutionValues)
        restitutionMean /= len(restitutionValues)

        print('Mean height: ' + str(heightMean))
        print('Restitution: ' + str(restitutionMean))

        restitutionVariance = 0
        for restitutionValue in restitutionValues:
            squaredError = math.pow(restitutionValue - restitutionMean,2)
            restitutionVariance += squaredError

        restitutionVariance /= len(restitutionValues)

        print('RestitutionVariance: ' + str(restitutionVariance))
        print()
