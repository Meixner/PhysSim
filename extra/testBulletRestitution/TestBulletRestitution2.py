import pybullet as p
import time
from PyQt5 import *
import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
import math
import numpy

radius = 0.005
useGUI = 0

if (useGUI): p.connect(p.GUI)
else: p.connect(p.DIRECT)
p.resetDebugVisualizerCamera(cameraDistance=1.2, cameraYaw=80, cameraPitch=-25, cameraTargetPosition=[-0.5,0,0.8])
p.setPhysicsEngineParameter(useSplitImpulse=1, restitutionVelocityThreshold=0) # Advanced feature, only when using maximal coordinates: split the positional constraint solving and velocity constraint solving in two stages, to prevent huge penetration recovery forces
p.setGravity(0, 0, -9.81)

colPlateId = p.createCollisionShape(p.GEOM_BOX, halfExtents=[0.30, 0.30, 0.05])
plateId = p.createMultiBody(baseMass=0, baseCollisionShapeIndex=colPlateId, baseVisualShapeIndex=-1, basePosition=[0,0,-0.05])
p.changeDynamics(colPlateId, -1, lateralFriction=1.0, spinningFriction=1.0, rollingFriction=1.0, linearDamping = 0, angularDamping = 0, restitution=1.0)

restitution = 0.8
minHeigth = 0.1
maxHeigth = 0.2
heigth = minHeigth
sphereColId = p.createCollisionShape(p.GEOM_SPHERE, radius=radius)
x = []
y = []
while (heigth <= maxHeigth):
    x.append(heigth)
    id = p.createMultiBody(baseMass=0.062, baseCollisionShapeIndex=sphereColId, baseVisualShapeIndex=-1, basePosition=[0,0.2,heigth])
    p.changeDynamics(id, -1, lateralFriction=0.1, spinningFriction=0.1, rollingFriction=0.1,
                     linearDamping=0, angularDamping=0, restitution=restitution)

    timestep = 0.002
    p.setTimeStep(timestep)

    currentTime = 0
    measure = 0
    spherePos, sphereOri = p.getBasePositionAndOrientation(id)
    h = spherePos[2]
    last = spherePos[2]
    max = 0
    times = 0

    while times < 1:
        p.stepSimulation()
        if (useGUI): time.sleep(timestep)

        currentTime += timestep
        spherePos, sphereOri = p.getBasePositionAndOrientation(id)
        if (spherePos[2] >= last):
            max = spherePos[2]
            measure = 1
        else:
            if (measure):
                restitutionValue = math.sqrt(max/h)
                y.append(restitutionValue)
                times += 1
                h = max
                measure = 0

        last = spherePos[2]

    p.removeBody(id)

    heigth += 0.00001
	print heigth

xNP = numpy.array(x)
yNP = numpy.array(y)
z = numpy.polyfit(xNP, yNP, 1)
f = numpy.poly1d(z)
xp = numpy.linspace(minHeigth, maxHeigth, 100)
plt.plot(xNP, yNP, '.', label='Measured values')
plt.plot([minHeigth, maxHeigth], [0.8,0.8], '--', label="Set restitution")

plt.xlabel('Drop_heigth')
plt.ylabel('Obverved restitution with formula')
plt.title('Observed restitution with formula sqrt(first_bounce_height / drop_height) on different heigths (Set resitution 0.8)')
plt.legend()
plt.show()