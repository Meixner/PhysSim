import pybullet as p
import time
from PyQt5 import *
import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
import Plot
import math
import numpy
from definitions import MODEL_DIR

radius = 0.005
#restitution = 0.2
#restitution = 0.5869966174692149
useGUI = 0

if (useGUI): p.connect(p.GUI)
else: p.connect(p.DIRECT)
p.resetDebugVisualizerCamera(cameraDistance=1.2, cameraYaw=80, cameraPitch=-25, cameraTargetPosition=[-0.5,0,0.8])
p.setPhysicsEngineParameter(useSplitImpulse=1, restitutionVelocityThreshold=0) # Advanced feature, only when using maximal coordinates: split the positional constraint solving and velocity constraint solving in two stages, to prevent huge penetration recovery forces
p.setGravity(0, 0, -9.81)

colPlateId = p.createCollisionShape(p.GEOM_BOX, halfExtents=[0.30, 0.30, 0.05])
plateId = p.createMultiBody(baseMass=0, baseCollisionShapeIndex=colPlateId, baseVisualShapeIndex=-1, basePosition=[0,0,-0.05])
p.changeDynamics(colPlateId, -1, lateralFriction=1.0, spinningFriction=1.0, rollingFriction=1.0, linearDamping = 0, angularDamping = 0, restitution=1.0)

restitution = 0.1
startHeights = [0.2]
sphereColId = p.createCollisionShape(p.GEOM_SPHERE, radius=radius)
for startHeight in startHeights:
    restitution = 0.1
    trajectories = []
    trajectory = []
    x = []
    y = []
    while (restitution < 1):
        y.append(restitution)
        #sphereColId = p.createCollisionShape(p.GEOM_MESH, meshScale=[2*radius / 10, 2*radius / 10, 2*radius / 10], fileName=MODEL_DIR + "/Kugel10mm.stl")
        id = p.createMultiBody(baseMass=0.062, baseCollisionShapeIndex=sphereColId, baseVisualShapeIndex=-1, basePosition=[0,0.2,startHeight])
        p.changeDynamics(id, -1, lateralFriction=0.1, spinningFriction=0.1, rollingFriction=0.1,
                         linearDamping=0, angularDamping=0, restitution=restitution)

        timestep = 0.002
        p.setTimeStep(timestep)

        currentTime = 0
        measure = 0
        spherePos, sphereOri = p.getBasePositionAndOrientation(id)
        heigth = spherePos[2]
        last = spherePos[2]
        max = 0
        times = 0

        while times < 1:
            p.stepSimulation()
            if (useGUI): time.sleep(timestep)

            currentTime += timestep
            spherePos, sphereOri = p.getBasePositionAndOrientation(id)
            if (spherePos[2] >= last):
                max = spherePos[2]
                measure = 1
            else:
                if (measure):
                    restitutionValue = math.sqrt(max/heigth)
                    trajectory.append((restitutionValue, restitution))
                    x.append(restitutionValue)
                    times += 1
                    print(str(restitution) + " " + str(restitutionValue))
                    heigth = max
                    measure = 0

            last = spherePos[2]

        p.removeBody(id)

        restitution += 0.02

    xNP = numpy.array(x)
    yNP = numpy.array(y)
    z = numpy.polyfit(xNP, yNP, 1)
    print(z)
    f = numpy.poly1d(z)
    xp = numpy.linspace(0.0, 1.2, 100)
    plt.plot(xNP, yNP, '.', label='Measured values - height ' + str(startHeight * 100) + 'cm')
    plt.plot(xp, f(xp), '-', label='Fitted linear function - height ' + str(startHeight * 100) + 'cm')


plt.xlabel('Obverved restitution with formula: sqrt(first_bounce_height / drop_height)')

help = numpy.linspace(0.0, numpy.max(xNP), 100)
plt.plot(help, help, '--', label="f(x)=x")
plt.ylabel('Set restitution of sphere')
plt.title('Difference of set restitution and observed restitution')
plt.legend()
plt.show()