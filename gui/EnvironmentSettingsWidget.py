import PhysSim
import sys
import Models
import pickle
from gui.ExperimentExecutionWidget import ExperimentExecutionWidget
from PyQt5.QtWidgets import *
from definitions import ENVIRONMENT_FILE
from pathlib import Path

class SettingsSpinner(QDoubleSpinBox):
    def __init__(self, envSetting):
        super(SettingsSpinner, self).__init__()
        self.setMinimum(envSetting.envType.value.minimum)
        self.setSingleStep(envSetting.envType.value.step)
        self.setMaximum(envSetting.envType.value.maximum)
        self.setDecimals(envSetting.envType.value.decimals)
        self.valueChanged.connect(envSetting.setValue)
        self.setValue(envSetting.value)

class EnvironmentSettingsWidget(QWidget):
    def __init__(self, physEngines):
        super(EnvironmentSettingsWidget, self).__init__()
        self.basePosition = [0, 0, 1]

        self.envSettings = 0
        if (Path(ENVIRONMENT_FILE).is_file()):
            filehandler = open(str(ENVIRONMENT_FILE), 'rb')
            self.envSettings, self.camSettings = pickle.load(filehandler)
        else:
            self.envSettings = self.initEnvSettings()
            self.camSettings = self.initCamSettings()

        self.settings = self.envSettings.settings
        self.physEngines = physEngines
        self.physSim = 0

        self.grid = QGridLayout()
        self.setLayout(self.grid)
        self.initUI()

        self.show()

    def initUI(self):
        self.setWindowTitle("Physical Simulation - Environment Settings")

        environment = QGroupBox("Experiment settings")
        environment.setLayout(QGridLayout())
        i = 0
        for key in self.envSettings.order:
            value = self.settings[key]
            label = QLabel(value.name)
            environment.layout().addWidget(label, *(i, 0))
            spin = SettingsSpinner(value)
            environment.layout().addWidget(spin, *(i, 1))
            i+=1
        self.grid.addWidget(environment, *(0,0))

        environment = QGroupBox("Camera settings")
        environment.setLayout(QGridLayout())
        i = 0
        for key in self.camSettings.order:
            value = self.camSettings.settings[key]
            label = QLabel(value.name)
            environment.layout().addWidget(label, *(i, 0))
            spin = SettingsSpinner(value)
            environment.layout().addWidget(spin, *(i, 1))
            i+=1
        self.grid.addWidget(environment, *(0,1))

        physEngineLayout = QHBoxLayout()
        physEngineLayout.addWidget(QLabel('Physic Engine'))
        self.physEngine = QComboBox()
        for name, engine in self.physEngines:
            self.physEngine.addItem(name, userData=engine)
        physEngineLayout.addWidget(self.physEngine)
        self.grid.addLayout(physEngineLayout, *(1,1))

        useGUILayout = QHBoxLayout()
        useGUILayout.addWidget(QLabel('Use GUI'))
        self.useGUI = QCheckBox()
        self.useGUI.setChecked(0)
        useGUILayout.addWidget(self.useGUI)
        self.grid.addLayout(useGUILayout, *(1,0))

        hLayout = QHBoxLayout()
        self.grid.addLayout(hLayout, *(3,0,1,0))

        createBtn = QPushButton('Create environment')
        createBtn.clicked.connect(self.createEnvironment)
        hLayout.addWidget(createBtn)

        saveBtn = QPushButton('Save environment')
        saveBtn.clicked.connect(self.saveEnvironment)
        hLayout.addWidget(saveBtn)

        resetBtn = QPushButton('Reset environment')
        resetBtn.clicked.connect(self.resetEnvironment)
        hLayout.addWidget(resetBtn)

    def createEnvironment(self):
        if (self.physSim):
            self.physSim.endSim()
        self.physSim = PhysSim.PhysSim(useGUI=self.useGUI.isChecked(), physEngine=self.physEngine.currentData(), basePosition=self.basePosition,  #plateFriction=self.settings["PlateFriction"].value, plateRestitution=self.settings["PlateRestitution"].value,
                                       plateSizeX= self.settings["PlateSizeX"].value, plateSizeY= self.settings["PlateSizeY"].value, plateSizeZ = self.settings["PlateSizeZ"].value,
                                       jointPlateDifference = self.settings["JointPlateDifference"].value, lowerPlateJointDifferenceY = self.settings["LowerPlateDiffY"].value, lowerPlateAngle = self.settings["LowerPlateAngle"].value,
                                       lowerUpperJointDifferenceY= self.settings["LowerUpperJointDiffY"].value, lowerUpperJointDifferenceZ = self.settings["LowerUpperJointDiffZ"].value,
                                       upperPlateJointDifferenceY = self.settings["UpperPlateJointDiffY"].value, upperPlateAngle = self.settings["UpperPlateAngle"].value,
                                       laneWidth = self.settings["LaneWidth"].value, laneHeight = self.settings["LaneBorderHeight"].value)

        self.physSim.initCamera(self.camSettings.settings["CoordinateZ"].value,self.camSettings.settings["CoordinateX'"].value,self.camSettings.settings["CoordinateZ''"].value,
                                self.camSettings.settings["LowerJointCameraDiffX"].value,self.camSettings.settings["LowerJointCameraDiffY"].value,self.camSettings.settings["LowerJointCameraDiffZ"].value,
                                self.camSettings.settings["FocalLength"].value,self.camSettings.settings["ResolutionW"].value,self.camSettings.settings["ResolutionH"].value,
                                self.camSettings.settings["RoiResolutionW"].value,self.camSettings.settings["RoiResolutionH"].value,self.camSettings.settings["RoiOffsetW"].value,
                                self.camSettings.settings["RoiOffsetH"].value,self.camSettings.settings["PixelSize"].value)

        self.Open = ExperimentExecutionWidget(self.physSim)
        self.close()

    def saveEnvironment(self):
        file_env = open(str(ENVIRONMENT_FILE), 'wb')
        pickle.dump((self.envSettings, self.camSettings), file_env)

    def resetEnvironment(self):
        self.envSettings = self.initEnvSettings()
        self.camSettings = self.initCamSettings()
        self.settings = self.envSettings.settings
        self.initUI()

    def initEnvSettings(self):
        return Models.EnvironmentSettings([("PlateSizeX", Models.EnvironmentSetting('Plate size (x-axis, in m)', 0.21, Models.EnvironmentType.MEASURE)),
            #("PlateFriction", Models.EnvironmentSetting('Plate friction', 1.0, Models.EnvironmentType.COEFF)),
            #("PlateRestitution", Models.EnvironmentSetting('Plate restitution', 1.0, Models.EnvironmentType.COEFF)),
            ("PlateSizeY", Models.EnvironmentSetting('Plate size (y-axis, in m)', 0.3, Models.EnvironmentType.MEASURE)),
            ("PlateSizeZ", Models.EnvironmentSetting('Plate size (z-axis, in m)', 0.055, Models.EnvironmentType.MEASURE)),
            ("JointPlateDifference", Models.EnvironmentSetting('Joint plate difference (z-axis, in m)', 0.0375, Models.EnvironmentType.MEASURE)),
            ("LowerPlateDiffY", Models.EnvironmentSetting('Lower plate joint difference (y-axis, in m)', -0.0575, Models.EnvironmentType.MEASURE)),
            ("LowerPlateAngle", Models.EnvironmentSetting('Lower plate angle', 20.7, Models.EnvironmentType.ANGLE)),
            ("LowerUpperJointDiffY", Models.EnvironmentSetting('Lower upper joint difference (y-axis, in m)', -0.35, Models.EnvironmentType.MEASURE)),
            ("LowerUpperJointDiffZ", Models.EnvironmentSetting('Lower upper joint difference (z-axis, in m)', 0.235, Models.EnvironmentType.MEASURE)),
            ("UpperPlateJointDiffY", Models.EnvironmentSetting('Upper plate joint difference (y-axis, in m)', 0.0375, Models.EnvironmentType.MEASURE)),
            ("UpperPlateAngle", Models.EnvironmentSetting('Upper plate angle', -21.7, Models.EnvironmentType.ANGLE)),
            ("LaneWidth", Models.EnvironmentSetting('Lane width (in m)', 0.012, Models.EnvironmentType.MEASURE)),
            ("LaneBorderHeight", Models.EnvironmentSetting('Lane border height (in m)', 0.0025, Models.EnvironmentType.MEASURE))])

    def initCamSettings(self):
        return Models.EnvironmentSettings([("CoordinateZ", Models.EnvironmentSetting('Coordinate system transformation z', -90, Models.EnvironmentType.ANGLE)),
                                           ("CoordinateX'", Models.EnvironmentSetting("Coordinate system transformation x'", 90, Models.EnvironmentType.ANGLE)),
                                           ("CoordinateZ''", Models.EnvironmentSetting("Coordinate system transformation z''", 0,Models.EnvironmentType.ANGLE)),
                                           ("LowerJointCameraDiffX", Models.EnvironmentSetting("Lower joint camera difference (x-axis, in m)", 0.26575, Models.EnvironmentType.MEASURE)),
                                           ("LowerJointCameraDiffY", Models.EnvironmentSetting("Lower joint camera difference (y-axis, in m)", -0.11, Models.EnvironmentType.MEASURE)),
                                           ("LowerJointCameraDiffZ", Models.EnvironmentSetting("Lower joint camera difference (z-axis, in m)", 0.113, Models.EnvironmentType.MEASURE)),
                                           ("FocalLength", Models.EnvironmentSetting("Focal length (in m)", 0.008, Models.EnvironmentType.MEASURE)),
                                           ("ResolutionW", Models.EnvironmentSetting("Resolution width (in px)", 2048, Models.EnvironmentType.RESOLUTION)),
                                           ("ResolutionH", Models.EnvironmentSetting("Resolution heigth (in px)", 1088, Models.EnvironmentType.RESOLUTION)),
                                           ("RoiResolutionW", Models.EnvironmentSetting("Region of interest resolution (width, in px)", 1088, Models.EnvironmentType.RESOLUTION)),
                                           ("RoiResolutionH", Models.EnvironmentSetting("Region of interest resolution (heigth, in px)", 950, Models.EnvironmentType.RESOLUTION)),
                                           ("RoiOffsetW", Models.EnvironmentSetting("Region of interest offset (width, in px)", 460, Models.EnvironmentType.RES_OFFSET)),
                                           ("RoiOffsetH", Models.EnvironmentSetting("Region of interest offset (heigth, in px)", -20, Models.EnvironmentType.RES_OFFSET)),
                                           ("PixelSize", Models.EnvironmentSetting("Pixel size (in m)", 0.0000055, Models.EnvironmentType.MEASURE))])