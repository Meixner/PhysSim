from PyQt5.QtWidgets import *
import Models
from pathlib import Path
import pickle
from definitions import MODEL_FILES, MATERIAL_FILE, name

class RunSettingsGroupBox(QGroupBox):
    def __init__(self, parent):
        super(RunSettingsGroupBox, self).__init__()
        self.parent = parent
        self.setTitle('Run Settings')

        grid = QGridLayout()
        self.setLayout(grid)

        index = 0
        grid.addWidget(QLabel('Lane length (in m)'), index,0)
        self.laneLength = QDoubleSpinBox()
        self.laneLength.setDecimals(4)
        self.laneLength.setSingleStep(0.001)
        self.laneLength.setValue(0.165)
        grid.addWidget(self.laneLength, index,1)

        index += 1
        grid.addWidget(QLabel('Step size (in s)'), index,0)
        self.timestep = QDoubleSpinBox()
        self.timestep.setDecimals(4)
        self.timestep.setSingleStep(0.001)
        self.timestep.setValue(0.001)
        grid.addWidget(self.timestep, index,1)

        index += 1
        forceLabel = QLabel('Maximum deviation force')
        forceLabel.setToolTip('Maximum random deviation force of object to lane border')
        grid.addWidget(forceLabel, index,0)
        self.force = QDoubleSpinBox()
        self.force.setDecimals(2)
        self.force.setMinimum(0.0)
        self.force.setMaximum(100.0)
        self.force.setToolTip('Maximum random deviation force of object to lane border')
        self.force.setSingleStep(0.1)
        self.force.setValue(10.0)
        grid.addWidget(self.force, index,1)

        index += 1
        force2Label = QLabel('Maximum speed force')
        force2Label.setToolTip('Maximum random start speed force of object')
        grid.addWidget(force2Label, index,0)
        self.force2 = QDoubleSpinBox()
        self.force2.setDecimals(2)
        self.force2.setMinimum(0.0)
        self.force2.setMaximum(100.0)
        self.force2.setToolTip('Maximum random start speed force of object')
        self.force2.setSingleStep(0.1)
        self.force2.setValue(30.0)
        grid.addWidget(self.force2, index,1)

        index += 1
        self.materialGroupBox = MaterialsGroupBox()
        grid.addWidget(self.materialGroupBox, index, 0, 1, 2)

        index += 1
        runExperimentButton = QPushButton('Run')
        runExperimentButton.clicked.connect(self.run)
        grid.addWidget(runExperimentButton, index,0)
        self.runs = QSpinBox()
        self.runs.setMinimum(1)
        self.runs.setMaximum(10000)
        self.runs.setValue(100)
        self.runs.setToolTip('Amount of runs')
        grid.addWidget(self.runs, index,1)

    def run(self):
        if (self.materialGroupBox.selectedMaterial):
            self.parent.run(self.laneLength.value(), self.timestep.value(), self.materialGroupBox.selectedMaterial, self.force.value(), self.force2.value(), self.runs.value())
        else:
            print('No material selected')

class MaterialsGroupBox(QGroupBox):
    def __init__(self):
        super(MaterialsGroupBox, self).__init__()
        self.setTitle('Materials')
        self.selectedMaterial = 0

        boxLayout = QVBoxLayout()
        self.setLayout(boxLayout)

        addMaterialButton = QPushButton('Add Material')
        addMaterialButton.clicked.connect(self.addMaterial)
        boxLayout.addWidget(addMaterialButton)

        saveMaterialsButton = QPushButton('Save Materials')
        saveMaterialsButton.clicked.connect(self.saveMaterials)
        boxLayout.addWidget(saveMaterialsButton)

        self.materialList = QListWidget()
        self.materialList.setMinimumHeight(300)
        self.materialList.itemSelectionChanged.connect(self.materialSelectionChanged)
        boxLayout.addWidget(self.materialList)

        self.materialInfoLayout = QVBoxLayout()
        boxLayout.addLayout(self.materialInfoLayout)

        self.loadMaterials()
        if len(self.materialList.children()) > 0:
            self.materialList.setCurrentRow(0)

    def addMaterial(self):
        self.Open = AddMaterialWidget(self)

    def add(self, material):
        item = MaterialListWidgetItem(material)
        self.materialList.addItem(item)

    def loadMaterials(self):
        if (Path(MATERIAL_FILE).is_file()):
            filehandler = open(str(MATERIAL_FILE), 'rb')
            materials = pickle.load(filehandler)
            for material in sorted(materials, key=lambda material: material.displayName):
                self.add(material)

    def materialSelectionChanged(self):
        self.clearInfoBox()
        if len(self.materialList.selectedItems()) > 0:
            materialItem = self.materialList.selectedItems()[0]
            self.materialInfoLayout.addWidget(MaterialInfoGroupBox(self, materialItem))
            self.selectedMaterial = materialItem.material

    def materials(self):
        materials = []
        for i in range(self.materialList.count()):
            materials.append(self.materialList.item(i).material)
        return materials

    def saveMaterials(self):
        file_materials = open(str(MATERIAL_FILE), 'wb')
        pickle.dump(self.materials(), file_materials)

    def clearInfoBox(self):
        for i in reversed(range(self.materialInfoLayout.count())):
            widgetToRemove = self.materialInfoLayout.itemAt(i).widget()
            self.materialInfoLayout.removeWidget(widgetToRemove)
            widgetToRemove.setParent(None)

    def removeMaterial(self, materialItem):
        for i in range(self.materialList.count()):
            if self.materialList.item(i) == materialItem:
                self.materialList.takeItem(i)

class MaterialInfoGroupBox(QGroupBox):
    def __init__(self, parent, materialItem):
        super(MaterialInfoGroupBox, self).__init__()
        self.setTitle('Info')
        self.parent = parent
        self.materialItem = materialItem
        material = materialItem.material

        boxLayout = QVBoxLayout()
        self.setLayout(boxLayout)

        boxLayout.addWidget(QLabel(material.name))
        boxLayout.addWidget(QLabel('Mass: ' + str(material.mass * 1000) + 'g'))
        boxLayout.addWidget(QLabel('Friction: ' + str(material.friction)))
        boxLayout.addWidget(QLabel('Restitution: ' + str(material.restitution)))
        boxLayout.addWidget(QLabel('Restitution variance: ' + str(material.restitutionVariance)))
        boxLayout.addWidget(QLabel('Radius: ' + str(material.radius * 1000) + 'mm'))
        boxLayout.addWidget(QLabel('Model: ' + name(material.modelFilePath)))

        removeMaterialButton = QPushButton('Remove Material')
        removeMaterialButton.clicked.connect(self.removeMaterial)
        boxLayout.addWidget(removeMaterialButton)

    def removeMaterial(self):
        self.parent.removeMaterial(self.materialItem)

class MaterialListWidgetItem(QListWidgetItem):
    def __init__(self, material):
        super(MaterialListWidgetItem, self).__init__()
        self.material = material
        self.setText(material.displayName)

class ModelListWidgetItem(QListWidgetItem):
    def __init__(self, modelFilePath):
        super(ModelListWidgetItem, self).__init__()
        self.modelFilePath = modelFilePath
        self.setText(name(modelFilePath))

    def createEnding(self, radius):
        return ' - ' + name(self.modelFilePath)[:5] + str(2 * radius) + 'mm'

class AddMaterialWidget(QWidget):
    def __init__(self, parent):
        super(AddMaterialWidget, self).__init__()
        self.parent = parent
        self.setWindowTitle('Add new material')
        grid = QGridLayout()
        self.setLayout(grid)

        index = 0
        self.name = QLineEdit('MATERIAL_NAME')
        grid.addWidget(self.name, index, 0)
        self.nameLabel = QLabel('')
        grid.addWidget(self.nameLabel, index, 1)

        index += 1
        grid.addWidget(QLabel('Mass (in g)'), index, 0)
        self.mass = QDoubleSpinBox()
        self.mass.setDecimals(2)
        self.mass.setSingleStep(1)
        self.mass.setValue(50)
        grid.addWidget(self.mass, index, 1)

        index += 1
        grid.addWidget(QLabel('Friction coeff.'), index, 0)
        self.friction = QDoubleSpinBox()
        self.friction.setDecimals(5)
        self.friction.setSingleStep(0.01)
        self.friction.setMinimum(0.01)
        self.friction.setValue(0.01)
        grid.addWidget(self.friction, index, 1)

        index += 1
        grid.addWidget(QLabel('Restitution coeff.'), index, 0)
        self.restitution = QDoubleSpinBox()
        self.restitution.setDecimals(5)
        self.restitution.setSingleStep(0.01)
        self.restitution.setMinimum(0.01)
        self.restitution.setMaximum(1.0)
        self.restitution.setValue(0.8)
        grid.addWidget(self.restitution, index, 1)

        index += 1
        grid.addWidget(QLabel('Restitution variance'), index, 0)
        self.restitutionVariance = QDoubleSpinBox()
        self.restitutionVariance.setDecimals(5)
        self.restitutionVariance.setSingleStep(0.001)
        self.restitutionVariance.setMaximum(1.0)
        self.restitutionVariance.setValue(0.0)
        grid.addWidget(self.restitutionVariance, index, 1)

        index += 1
        grid.addWidget(QLabel('Radius (in mm)'), index, 0)
        self.radius = QDoubleSpinBox()
        self.radius.setDecimals(2)
        self.radius.setSingleStep(1)
        self.radius.setMaximum(100)
        self.radius.setValue(5)
        grid.addWidget(self.radius, index, 1)

        index += 1
        modelGroupBox = QGroupBox('Shape')
        grid.addWidget(modelGroupBox, index, 0, 1, 2)
        self.modelListWidget = QListWidget()
        self.modelListWidget.itemSelectionChanged.connect(self.modelChanged)
        modelLayout = QVBoxLayout()
        modelLayout.addWidget(self.modelListWidget)
        modelGroupBox.setLayout(modelLayout)
        for modelFilePath in MODEL_FILES:
            modelListWidgetItem = ModelListWidgetItem(modelFilePath)
            self.modelListWidget.addItem(modelListWidgetItem)
        self.modelListWidget.addItem(ModelListWidgetItem("Sphere"))
        if len(self.modelListWidget.children()) > 0:
            self.modelListWidget.setCurrentRow(0)

        index += 1
        addButton = QPushButton('Add')
        addButton.clicked.connect(self.add)
        grid.addWidget(addButton, index,0)

        cancelButton = QPushButton('Cancel')
        cancelButton.clicked.connect(self.close)
        grid.addWidget(cancelButton, index,1)

        self.show()

    def add(self):
        if len(self.modelListWidget.selectedItems()) > 0:
            modelListWidgetItem = self.modelListWidget.selectedItems()[0]
            displayName = self.name.text() + modelListWidgetItem.createEnding(self.radius.value())
            material = Models.Material(displayName, self.name.text(), self.mass.value() / 1000.0, self.friction.value(), self.restitution.value(), self.restitutionVariance.value(), self.radius.value() / 1000.0, str(modelListWidgetItem.modelFilePath))
            self.parent.add(material)
            print(displayName + ' added!')
        else:
            print('No model selected')

    def modelChanged(self):
        if len(self.modelListWidget.selectedItems()) > 0:
            modelListWidgetItem = (self.modelListWidget.selectedItems()[0])
            self.nameLabel.setText(modelListWidgetItem.createEnding(self.radius.value()))