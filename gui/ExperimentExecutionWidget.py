from gui.RunSettingsGroupBox import RunSettingsGroupBox
from gui.RunsGroupBox import RunsGroupBox
import PhysSim
import sys
from PyQt5.QtWidgets import *

class ExperimentExecutionWidget(QWidget):
    def __init__(self, physSim):
        super(ExperimentExecutionWidget, self).__init__()
        self.physSim = physSim
        self.setWindowTitle("Experiment Execution")

        boxLayout = QHBoxLayout()
        self.setLayout(boxLayout)

        runSettingsGroupBox = RunSettingsGroupBox(self)
        runSettingsGroupBox.setMinimumWidth(480)
        boxLayout.addWidget(runSettingsGroupBox)

        self.runsGroupBox = RunsGroupBox()
        self.runsGroupBox.setMinimumWidth(480)
        boxLayout.addWidget(self.runsGroupBox)

        self.show()

    def run(self, laneLength, timestep, material, maxDeviationForce, maxSpeedForce, runs):
        print('Executing ' + str(runs) + 'x run simulations for ' + material.name)

        trajectories, allContactPoints = self.physSim.runExperiment(modelPath=material.modelFilePath, mass=material.mass, friction=material.friction,
                              restitution=material.restitution, restitutionVariance=material.restitutionVariance, radius=material.radius, timestep=timestep,
                              laneLength=laneLength, amount=runs, maxDeviationForce=maxDeviationForce, maxSpeedForce=maxSpeedForce)

        self.runsGroupBox.addExecutedRun(material.displayName, trajectories, allContactPoints, material, laneLength, timestep, maxDeviationForce, maxSpeedForce, runs)