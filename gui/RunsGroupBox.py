from PyQt5.QtWidgets import *
import csv
import Plot
import Models
from definitions import name

class ExecutedRunsListWidgetItem(QListWidgetItem):
    def __init__(self, executedRun):
        super(ExecutedRunsListWidgetItem, self).__init__()
        self.setText(executedRun.name)
        self.executedRun = executedRun

class RunsGroupBox(QGroupBox):
    def __init__(self):
        super(RunsGroupBox, self).__init__()
        self.setTitle('Executed Runs')

        boxLayout = QVBoxLayout()
        self.setLayout(boxLayout)

        importTrajButton = QPushButton('Import trajectectory')
        importTrajButton.clicked.connect(self.importTrajectories)
        boxLayout.addWidget(importTrajButton)

        exportTrajButton = QPushButton('Export trajectory')
        exportTrajButton.clicked.connect(self.exportTrajectories)
        boxLayout.addWidget(exportTrajButton)

        self.runs = QListWidget()
        self.runs.setSelectionMode(QAbstractItemView.ExtendedSelection)
        boxLayout.addWidget(self.runs)

        plotTrajButton = QPushButton('Plot trajectectories')
        plotTrajButton.clicked.connect(self.plotTrajectories)
        boxLayout.addWidget(plotTrajButton)

        plotContactButton = QPushButton('Plot contact points')
        plotContactButton.clicked.connect(self.plotContacts)
        boxLayout.addWidget(plotContactButton)

    def addExecutedRun(self, materialName, trajectories, allContactPoints, material, laneLength, timestep, maxDeviationForce, maxSpeedForce, runs):
        item = ExecutedRunsListWidgetItem(Models.ExecutedRun(materialName, trajectories, allContactPoints, material, laneLength, timestep, maxDeviationForce, maxSpeedForce, runs))
        self.runs.addItem(item)
        self.runs.setCurrentItem(item)

    def importTrajectories(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName = QFileDialog.getOpenFileName(self, 'Import trajectory', '', 'CSV (*.csv)', options=options)
        if len(fileName[0]) > 0:
            with open(fileName[0]) as csv_file:
                reader = csv.DictReader(csv_file, delimiter=";")
                currentObjectId = -1;
                currentStartTimestamp = 0;
                material = ""
                shape = ""
                radius = 0.0;
                trajectory = []
                trajectories = []

                for row in reader:
                    if (row['timestamp_counter'] == "0" and row['object_id'] != "-1"):
                        objectId = int(row['object_id'])
                        if (objectId != currentObjectId):
                            currentObjectId = objectId
                            currentStartTimestamp = float(row['timestamp'])
                            material = (row['material'])
                            shape = row['shape']
                            radius = float(row['shape_size'].replace("mm", "")) / 2.0
                            if (len(trajectory) > 0):
                                trajectories.append(trajectory)
                                trajectory = []
                        #timestep = int(row['timestamp']) - currentStartTimestamp #ignore at first
                        x = float(row['center_x'])
                        y = float(row['center_y'])
                        trajectory.append((x,y))
                trajectories.append(trajectory)
                displayName = material + " - " + shape[:5] + str(radius * 2.0) + "mm"
                self.addExecutedRun(displayName + " [IMPORTED]", trajectories, [], Models.Material(displayName, material, 0, 0, 0, 0, radius, ''), 0.165, 0.001, 0, 0, currentObjectId + 1)

    def exportTrajectories(self):
        for item in list(self.runs.selectedItems()):
            executedRun = item.executedRun
            currentTimestep = 0
            object_id = 0
            timestep = executedRun.timestep
            options = QFileDialog.Options()
            options |= QFileDialog.DontUseNativeDialog
            fileName = QFileDialog.getSaveFileName(self, 'Export trajectory ' + item.executedRun.name, item.executedRun.name + '.csv', 'CSV (*.csv)', options=options)
            if len(fileName[0]) > 0:
                with open(fileName[0], 'w') as csv_file:
                    writer = csv.writer(csv_file, delimiter=';')
                    writer.writerow(['timestamp', 'timestamp_counter', 'object_id', 'center_x', 'center_y', 'radius', 'material', 'shape', 'shape_size'])
                    for trajectory in executedRun.trajectories:
                        for point in trajectory:
                            writer.writerow([currentTimestep, 0, object_id, point[0], point[1], '', executedRun.material.name, name(executedRun.material.modelFilePath), str(2.0 * executedRun.material.radius * 1000) + 'mm'])
                            currentTimestep += timestep
                        object_id += 1

    def plotTrajectories(self):
        plot = Plot.TrajectoryPlot('Trajectories')
        for item in list(self.runs.selectedItems()):
            plot.plot(item.executedRun.trajectories, item.executedRun.name)
        plot.show()

    def plotContacts(self):
        plot = Plot.ContactPoints('Contact points')
        for item in list(self.runs.selectedItems()):
            plot.plot(item.executedRun.allContactPoints, item.executedRun.material_name)
        plot.show()
