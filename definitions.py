import os
from pathlib import Path

ROOT_DIR = Path(os.path.dirname(os.path.abspath(__file__))) # Project Root

MODEL_DIR = ROOT_DIR / 'resources/objectModels'

MODEL_FILES = [MODEL_DIR / f for f in os.listdir(str(MODEL_DIR)) if f.endswith('.stl')]

EXPERIMENT_DIR = ROOT_DIR / 'resources/materialRestitutionExperiment'

EXPERIMENT_FILES = [EXPERIMENT_DIR / f for f in os.listdir(str(EXPERIMENT_DIR)) if f.endswith('.txt')]

MATERIAL_FILE = ROOT_DIR / 'resources/saveFiles/materials.obj'

ENVIRONMENT_FILE = ROOT_DIR / 'resources/saveFiles/environment.obj'

def name(filePath):
    return os.path.basename(str(filePath))
