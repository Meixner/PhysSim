from enum import Enum

class Material():
    def __init__(self, displayName, name, mass, friction, restitution, restitutionVariance, radius, modelFilePath):
        self.displayName = displayName
        self.name = name
        self.mass = mass #kg
        self.friction = friction
        self.restitution = restitution
        self.restitutionVariance = restitutionVariance
        self.radius = radius #m
        self.modelFilePath = modelFilePath

class ExecutedRun():
    def __init__(self, material_name, trajectories, allContactPoints, material, laneLength, timestep, maxDeviationForce, maxSpeedForce, runs):
        self.runs = len(trajectories)
        self.name = '#' + str(self.runs) + ' - ' + material_name
        self.material_name = material_name
        self.trajectories = trajectories
        self.allContactPoints = allContactPoints
        self.material = material
        self.laneLength = laneLength
        self.timestep = timestep
        self.maxSpeedForce = maxSpeedForce
        self.maxDeviationForce = maxDeviationForce
        self.runs = runs

class Type():
    def __init__(self, minimum, maximum, decimals, step):
        self.minimum = minimum
        self.maximum = maximum
        self.decimals = decimals
        self.step = step

    def __eq__(self, other):
        return self.minimum == other.minimum and self.maximum == other.maximum and self.decimals == other.decimals and self.step == other.step

    def __ne__(self, other):
        return self.minimum != other.minimum or self.maximum != other.maximum or self.decimals != other.decimals or self.step != other.step

class EnvironmentType(Enum):
    COEFF = Type(0.01, 1.0, 2, 0.01)
    ANGLE = Type(-90.0, 90.0, 2, 1.0)
    MEASURE = Type(-10.0, 10.0, 7, 0.001)
    RESOLUTION = Type(0, 5000, 0, 10)
    RES_OFFSET = Type(-5000, 5000, 0, 10)

class EnvironmentSetting():
    def __init__(self, name, initValue, envType):
        self.name = name
        self.initValue = initValue
        self.envType = envType
        self.value = initValue

    def setValue(self, value):
        self.value = value

class EnvironmentSettings():
    def __init__(self, settingTuples):
        self.order = []
        self.settings = {}
        for key, value in settingTuples:
            self.order.append(key)
            self.settings[key] = value

