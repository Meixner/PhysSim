import pybullet as p
import time

class BulletPhysSim(object):
    def init(self, useGUI, lowerJointPosition, plateSizeX, plateSizeY, plateSizeZ, jointPlateDifference, lowerPlateJointDifferenceY, plateJointDifferenceZ, lowerPlateAngleRad, upperJointPosition, upperPlateJointDifferenceY, upperPlateAngleRad, laneWidth, laneHeight):
        self.useGUI = useGUI
        if useGUI:
            p.connect(p.GUI)
            p.resetDebugVisualizerCamera(cameraDistance=1.2, cameraYaw=80, cameraPitch=-25, cameraTargetPosition=[-0.5,0,0.8])
        else:
            p.connect(p.DIRECT)
        p.setPhysicsEngineParameter(useSplitImpulse=1, restitutionVelocityThreshold=0)
        p.setGravity(0, 0, -9.81)

        colPlateId = p.createCollisionShape(p.GEOM_BOX, halfExtents=[plateSizeX / 2, plateSizeY / 2, plateSizeZ / 2])
        colJointId = p.createCollisionShape(p.GEOM_BOX, halfExtents=[plateSizeX / 2, jointPlateDifference, jointPlateDifference])

        self.lowerId = p.createMultiBody(baseMass=0,
                                         baseCollisionShapeIndex=colJointId,
                                         baseVisualShapeIndex=-1,
                                         basePosition=lowerJointPosition,
                                         linkCollisionShapeIndices=[colPlateId],
                                         linkMasses=[0],
                                         linkVisualShapeIndices=[-1],
                                         linkPositions=[[0, lowerPlateJointDifferenceY, plateJointDifferenceZ]],
                                         linkOrientations=[[0, 0, 0, 1]],
                                         linkInertialFramePositions=[[0, 0, 0]],
                                         linkInertialFrameOrientations=[[0, 0, 0, 1]],
                                         linkParentIndices=[0],
                                         linkJointAxis=[[0, 0, 1]],
                                         linkJointTypes=[p.JOINT_REVOLUTE],
                                         baseOrientation=p.getQuaternionFromEuler([lowerPlateAngleRad, 0, 0]))
        p.changeDynamics(self.lowerId, 0, lateralFriction=1.0, spinningFriction=1.0, rollingFriction=1.0, restitution=1.0) # restitution and friction are multiplied and therefore only the sphere matters

        colLaneBorderId = p.createCollisionShape(p.GEOM_BOX, halfExtents=[laneWidth / 2, plateSizeY / 2, (plateSizeZ + laneHeight) / 2])
        upperId = p.createMultiBody(baseMass=0,
                                    baseCollisionShapeIndex=colJointId,
                                    baseVisualShapeIndex=-1,
                                    basePosition=upperJointPosition,
                                    linkCollisionShapeIndices=[colPlateId, colLaneBorderId, colLaneBorderId],
                                    linkMasses=[0, 0, 0],
                                    linkVisualShapeIndices=[-1, -1, -1],
                                    linkPositions=[[0, upperPlateJointDifferenceY, plateJointDifferenceZ], [laneWidth, 0, 0], [-laneWidth, 0, 0]],
                                    linkOrientations=[[0, 0, 0, 1], [0, 0, 0, 1], [0, 0, 0, 1]],
                                    linkInertialFramePositions=[[0, 0, 0], [0, 0, 0], [0, 0, 0]],
                                    linkInertialFrameOrientations=[[0, 0, 0, 1], [0, 0, 0, 1], [0, 0, 0, 1]],
                                    linkParentIndices=[0, 1, 1],
                                    linkJointAxis=[[0, 0, 1], [0, 0, 1], [0, 0, 1]],
                                    linkJointTypes=[p.JOINT_REVOLUTE, p.JOINT_REVOLUTE, p.JOINT_REVOLUTE],
                                    baseOrientation=p.getQuaternionFromEuler([upperPlateAngleRad, 0, 0]))
        p.changeDynamics(upperId, 0, lateralFriction=1.0, spinningFriction=1.0, rollingFriction=1.0, restitution=0.0) # no bounciness on upper plate

    def runExperiment(self, modelPath, radius, mass, friction, restitution, startPosition, startOrientation, force, timestep, startHeigth, endHeight):
        bulletCol = 1 # modelPath == "Sphere" # currently not needed anymore
        colId = self.createSphereCol(radius) if bulletCol else self.createColModel(radius, modelPath)

        if (not(bulletCol)): startPosition = [startPosition[0] - radius, startPosition[1], startPosition[2] - radius] # position is not in the center for meshs

        p.setTimeStep(timestep)

        id = p.createMultiBody(baseMass=mass, baseCollisionShapeIndex=colId, baseVisualShapeIndex=-1,
                               basePosition=startPosition, baseInertialFrameOrientation=startOrientation)
        #restitution = 1.02042017 * restitution - 0.08793547 # boost restitution is too high

        p.changeDynamics(id, -1, lateralFriction=friction, spinningFriction=friction, rollingFriction=friction,
                         linearDamping=1e-5, angularDamping=1e-5, restitution=restitution)

        p.applyExternalForce(id, -1, force, startPosition, p.WORLD_FRAME)

        trajectory = []
        contactPoints = []

        while 1:
            p.stepSimulation()
            if (self.useGUI): time.sleep(timestep)

            spherePos, sphereOri = p.getBasePositionAndOrientation(id)
            if (not (bulletCol)): spherePos = [spherePos[0] + radius, spherePos[1], spherePos[2] + radius]  # position is not in the center for meshs
            if (spherePos[2] < startHeigth): break

        spherePos, sphereOri = p.getBasePositionAndOrientation(id)
        if (not (bulletCol)): spherePos = [spherePos[0] + radius, spherePos[1], spherePos[2] + radius]  # position is not in the center for meshs
        trajectory.append((spherePos[0], spherePos[1], spherePos[2]))
        currentTime = 0.0
        firstContact = 0
        contactTime = 0

        while (1):
            p.stepSimulation()
            if (self.useGUI): time.sleep(timestep)

            currentTime += timestep
            spherePos, sphereOri = p.getBasePositionAndOrientation(id)
            if (not (bulletCol)): spherePos = [spherePos[0] + radius, spherePos[1], spherePos[2] + radius]  # position is not in the center for meshs
            trajectory.append((spherePos[0], spherePos[1], spherePos[2]))
            cPoints = p.getContactPoints(bodyA=id, bodyB=self.lowerId)
            for val in cPoints:
                if val[8] < 0.001:
                    if not firstContact:
                        contactPoints.append((val[5][2], val[5][0]))
                        firstContact = 1
                        contactTime = currentTime
                    else:
                        if (currentTime - contactTime) > (10 * timestep):
                            contactTime = currentTime
                            contactPoints.append((val[5][2], val[5][0]))
                    break
            if (spherePos[2] < endHeight): break

        p.removeBody(id)

        return trajectory, contactPoints

    def createSphereCol(self, radius):
        return p.createCollisionShape(p.GEOM_SPHERE, radius=radius)

    def createColModel(self, radius, modelFilePath):
        return p.createCollisionShape(p.GEOM_MESH, meshScale=[2*radius / 10, 2*radius / 10, 2*radius / 10], fileName=modelFilePath)

    def endSim(self):
        p.disconnect()
