#ifndef IENGINE
#define IENGINE

#include <boost/python.hpp>
#include <vector>
#include <string>

namespace PhysEngine
{
    class IEngine
    {
        virtual void init(bool useGUI, boost::python::list& lowerBarPosition, float plateSizeX,
                          float plateSizeY, float plateSizeZ, float jointPlateDifference,
                          float lowerPlateBarDifferenceY, float plateBarDifferenceZ,
                          float lowerPlateAngleRad, boost::python::list& upperBarPosition,
                          float upperPlateBarDifferenceY, float upperPlateAngleRad,
                          float laneWidth, float laneHeigth) = 0;

        virtual boost::python::tuple
            runExperiment(std::string modelPath, float radius, float mass, float friction,
                          float restitution, boost::python::list& startPosition,
                          boost::python::tuple& startOrientationQuaternion, boost::python::list& force,
                          float timestep, float startHeight, float endHeight) = 0;

        virtual void endSim() = 0;
    };
}

#endif // IENGINE

