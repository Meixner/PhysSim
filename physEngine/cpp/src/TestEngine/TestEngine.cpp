#include "TestEngine.h"

#include <iostream>

using namespace PhysEngine;

void TestEngine::init(bool useGUI, boost::python::list& lowerBarPosition, float plateSizeX,
                      float plateSizeY, float plateSizeZ, float jointPlateDifference,
                      float lowerPlateBarDifferenceY, float plateBarDifferenceZ,
                      float lowerPlateAngleRad, boost::python::list& upperBarPosition,
                      float upperPlateBarDifferenceY, float upperPlateAngleRad,
                      float laneWidth, float laneHeigth) {
    std::cout << "init" << std::endl;
}

boost::python::tuple
    TestEngine::runExperiment(std::string modelPath, float radius, float mass, float friction,
                              float restitution, boost::python::list& startPosition,
                              boost::python::tuple& startOrientationQuaternion, boost::python::list& force,
                              float timestep, float startHeigth, float endHeight) {
    std::cout << "runExperiment" << std::endl;

    boost::python::list testTrajectory = boost::python::list();
    boost::python::list testContactPoints = boost::python::list();
    for (int i = 0; i < 100; i++) {
        boost::python::list testPoint = boost::python::list();
        for (int j = 0; j < 3; j++) {
            testPoint.append(i * j);
        }
        testTrajectory.append(testPoint);
        testContactPoints.append(testPoint);
    }

    return boost::python::make_tuple(testTrajectory,testContactPoints);
}

void TestEngine::endSim() {
    std::cout << "endSim" << std::endl;
}

