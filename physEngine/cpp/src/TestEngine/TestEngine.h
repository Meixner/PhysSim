#ifndef TESTENGINE_H
#define TESTENGINE_H

#include "../IEngine.h"

namespace PhysEngine
{
    struct TestEngine : IEngine
    {
        void init(bool useGUI, boost::python::list& lowerBarPosition, float plateSizeX,
                      float plateSizeY, float plateSizeZ, float jointPlateDifference,
                      float lowerPlateBarDifferenceY, float plateBarDifferenceZ,
                      float lowerPlateAngleRad, boost::python::list& upperBarPosition,
                      float upperPlateBarDifferenceY, float upperPlateAngleRad,
                      float laneWidth, float laneHeigth);

        boost::python::tuple
            runExperiment(std::string modelPath, float radius, float mass, float friction,
                          float restitution, boost::python::list& startPosition,
                          boost::python::tuple& startOrientationQuaternion, boost::python::list& force,
                          float timestep, float startHeigth, float endHeight);

        void endSim();
    };
}

#endif // TESTENGINE_H
