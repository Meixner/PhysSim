#include <boost/python.hpp>
#include "TestEngine/TestEngine.h"

using namespace boost::python;

BOOST_PYTHON_MODULE(cppProject) {

    class_<PhysEngine::TestEngine>("TestEngine")
            .def("init", &PhysEngine::TestEngine::init)
            .def("runExperiment", &PhysEngine::TestEngine::runExperiment)
            .def("endSim", &PhysEngine::TestEngine::endSim);
}
