import random
import matplotlib
from matplotlib import colors as mcolors
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt

colors = ['b', 'r', 'g', 'm', 'c', 'y']
#colors = [color for color in mcolors.CSS4_COLORS if mcolors.rgb_to_hsv(mcolors.to_rgb(color))[1] > 0.9 and mcolors.rgb_to_hsv(mcolors.to_rgb(color))[2] > 0.9]

class TrajectoryPlot:
    def __init__(self, title):
        plt.close()
        self.title = title
        self.figure = plt.figure(1, figsize=[10,10])
        self.ax = plt.subplot(111)
        self.colorIndex = 0

    def plot(self, trajectories, label):
        alpha = max(4.0 / (trajectories.__len__() + 3), 0.1)
        color = colors[self.colorIndex]
        self.colorIndex += 1
        if self.colorIndex >= len(colors): self.colorIndex = 0
        self.ax.plot([],[],alpha=1.0, linestyle='dashed', linewidth=2, color=color, label = label)
        for trajectory in trajectories:
            x = []
            y = []
            for point in trajectory:
                x.append(point[0])
                y.append(point[1])
            self.ax.plot(x, y, alpha=alpha, linestyle='dashed', linewidth=2, color=color)

    def show(self):
        plt.title(self.title)
        plt.xlabel('x (in pixel)')
        plt.ylabel('y (in pixel)')
        plt.gca().invert_yaxis()
        self.ax.legend()
        self.figure.tight_layout()
        plt.show()


class ContactPoints:
    def __init__(self, title):
        plt.close()
        self.title = title
        self.figure = plt.figure(1, figsize=[10,10])
        self.ax = plt.subplot(111)
        self.colorIndex = 0

    def plot(self, allContactPoints, label, amount = 5):
        color = colors[self.colorIndex]
        self.colorIndex += 1
        if self.colorIndex >= len(colors): self.colorIndex = 0
        index = 0
        labeled = 0
        while (index < amount):
            for contactPoints in allContactPoints:
                if (len(contactPoints) > index):
                    if (labeled):
                        self.ax.scatter(contactPoints[index][0], contactPoints[index][1], marker='o', color=color)
                    else:
                        self.ax.scatter(contactPoints[index][0], contactPoints[index][1], marker='o', color=color, label=label)
                        labeled = 1
            hsvColor = mcolors.rgb_to_hsv(mcolors.to_rgb(color))
            hsvColor[1] = 0.5 * hsvColor[1]
            color = mcolors.hsv_to_rgb(hsvColor)
            index += 1

    def show(self):
        plt.title(self.title)
        plt.xlabel('y (in metre)')
        plt.ylabel('x (in metre)')
        self.ax.legend()
        self.figure.tight_layout()
        plt.show()