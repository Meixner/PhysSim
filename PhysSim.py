import pybullet as p
import math
import random
import numpy as np

def toRad(angle):
    return angle / 360.0 * 2 * math.pi

class PhysSim(object):
    def __init__(self, physEngine, useGUI, basePosition, plateSizeX, plateSizeY, plateSizeZ, jointPlateDifference, lowerPlateJointDifferenceY, lowerPlateAngle, lowerUpperJointDifferenceY, lowerUpperJointDifferenceZ, upperPlateJointDifferenceY, upperPlateAngle, laneWidth, laneHeight):
        self.useGUI = useGUI
        self.basePosition = basePosition
        self.plateSizeX = plateSizeX
        self.plateSizeY = plateSizeY
        self.plateSizeZ = plateSizeZ
        self.jointPlateDifference = jointPlateDifference
        self.laneWidth = laneWidth
        self.lowerPlateAngle = toRad(lowerPlateAngle)
        self.lowerPlateJointDifferenceY = lowerPlateJointDifferenceY
        self.upperPlateAngle = toRad(upperPlateAngle)
        self.upperPlateJointDifferenceY = upperPlateJointDifferenceY
        self.plateJointDifferenceZ = jointPlateDifference + plateSizeZ / 2  # difference of center (y-Axis)
        self.upperJointPosition = [basePosition[0], basePosition[1] + lowerUpperJointDifferenceY, basePosition[2] + lowerUpperJointDifferenceZ]

        self.physicalSimulation = physEngine
        self.physicalSimulation.init(useGUI, basePosition, plateSizeX, plateSizeY, plateSizeZ,
                                     jointPlateDifference, lowerPlateJointDifferenceY, self.plateJointDifferenceZ,
                                     self.lowerPlateAngle, self.upperJointPosition, self.upperPlateJointDifferenceY,
                                     self.upperPlateAngle, laneWidth, laneHeight)


    def calculateLaneEndPosition(self, laneLength, radius):
        laneEndPosition_NotRotated = [self.upperJointPosition[0], self.upperJointPosition[1] + self.upperPlateJointDifferenceY + self.plateSizeY / 2 - laneLength,
                                      self.upperJointPosition[2] + self.jointPlateDifference + self.plateSizeZ + radius]
        laneEndPosition_Rotated = [self.upperJointPosition[0],
                                   self.upperJointPosition[1] + math.cos(self.upperPlateAngle) * (laneEndPosition_NotRotated[1] - self.upperJointPosition[1])
                                    - math.sin(self.upperPlateAngle) * (laneEndPosition_NotRotated[2] - self.upperJointPosition[2]),
                                   self.upperJointPosition[2] + math.sin(self.upperPlateAngle) * (laneEndPosition_NotRotated[1] - self.upperJointPosition[1])
                                    + math.cos(self.upperPlateAngle) * (laneEndPosition_NotRotated[2] - self.upperJointPosition[2])]
        return laneEndPosition_Rotated

    def calculateLowerLaneEndPosition(self, laneLength, radius):
        laneEndPosition_NotRotated = [self.basePosition[0], self.basePosition[1] + self.lowerPlateJointDifferenceY + self.plateSizeY / 2 - (self.plateSizeY - laneLength),
                                      self.basePosition[2] + self.jointPlateDifference + self.plateSizeZ + radius]
        laneEndPosition_Rotated = [self.basePosition[0],
                                   self.basePosition[1] + math.cos(self.lowerPlateAngle) * (
                                               laneEndPosition_NotRotated[1] - self.basePosition[1])
                                   - math.sin(self.lowerPlateAngle) * (
                                               laneEndPosition_NotRotated[2] - self.basePosition[2]),
                                   self.basePosition[2] + math.sin(self.lowerPlateAngle) * (
                                               laneEndPosition_NotRotated[1] - self.basePosition[1])
                                   + math.cos(self.lowerPlateAngle) * (
                                               laneEndPosition_NotRotated[2] - self.basePosition[2])]
        return laneEndPosition_Rotated

    def runExperiment(self, modelPath, radius, mass, friction, restitution, restitutionVariance, laneLength, timestep, amount, maxDeviationForce, maxSpeedForce):
        topLeftPosition = self.calculateLaneEndPosition(0, radius)
        startHeight = topLeftPosition[2]
        endHeight = self.calculateLowerLaneEndPosition(0, radius)[2]

        laneEndPosition_Rotated = self.calculateLaneEndPosition(laneLength, radius)

        trajectories = []
        allContactPoints = []

        for i in range(0, amount, 1):
            print('Currently executing physical simulation ' + str(i + 1) + ' of ' + str(amount))
            deviation = (random.random() - 0.5) * (self.laneWidth - radius * 2)
            startPos = [laneEndPosition_Rotated[0] + deviation, laneEndPosition_Rotated[1], laneEndPosition_Rotated[2]]
            startOrientation = p.getQuaternionFromEuler([random.random() * math.pi, random.random() * math.pi, random.random() * math.pi])
            restitutionRand = random.gauss(restitution, restitutionVariance)
            print('  > Using restitution of ' + str(restitutionRand))
            directionVec = (np.subtract(topLeftPosition, laneEndPosition_Rotated)) / np.linalg.norm((np.subtract(topLeftPosition, laneEndPosition_Rotated)))
            speedForce = random.random() * maxSpeedForce
            force = [maxDeviationForce * random.random() - (maxDeviationForce * 0.5), speedForce * directionVec[1],speedForce * directionVec[2]]
            print('  > Using start force of ' + str(force))

            trajectory, contactPoints = self.physicalSimulation.runExperiment(str(modelPath), radius, mass, friction, restitutionRand, startPos, startOrientation, force, timestep, startHeight, endHeight)
            trajectories.append(trajectory)
            allContactPoints.append(contactPoints)

        return self.trajectoriesToCameraKoord(trajectories), allContactPoints

    def initCamera(self, alpha, beta, gamma, lowerJointCameraDifferenceX, lowerJointCameraDifferenceY, lowerJointCameraDifferenceZ, focal_length, resolutionW, resolutionH, roiResolutionW, roiResolutionH, roiOffsetW, roiOffsetH, pixel_size):
        cam = np.array([self.basePosition[0] + lowerJointCameraDifferenceX + self.plateSizeX / 2.0, self.basePosition[1] + lowerJointCameraDifferenceY, self.basePosition[2] + lowerJointCameraDifferenceZ])
        self.Rot = self.rotationMatrix(toRad(alpha), toRad(beta), toRad(gamma))
        self.translation = -self.Rot.dot(cam)
        self.focal_length = focal_length
        self.resolutionW = resolutionW
        self.resolutionH = resolutionH
        self.roiResolutionW = roiResolutionW
        self.roiResolutionH = roiResolutionH
        self.roiOffsetW = roiOffsetW
        self.roiOffsetH = roiOffsetH
        self.pixel_size = pixel_size

    def trajectoriesToCameraKoord(self, trajectories):
        trajectories_camKoord = []
        for trajectory in trajectories:
            trajectories_camKoord.append(self.trajectoryToCameraKoord(trajectory))
        return trajectories_camKoord

    def trajectoryToCameraKoord(self, trajectory):
        trajectory_camKoord = []
        for point in trajectory:
            point_cam = self.toCameraKoord(point)
            if (point_cam != 0):
                trajectory_camKoord.append(point_cam)
        return trajectory_camKoord

    def toCameraKoord(self, point):
        point_world = np.array([point[0], point[1], point[2]])

        point_camKoord = self.Rot.dot(point_world) + self.translation

        point_img = [(self.focal_length * point_camKoord[0]) / point_camKoord[2],
                    (self.focal_length * point_camKoord[1]) / point_camKoord[2]]

        u = (point_img[0] / (self.pixel_size) - self.roiOffsetW + self.resolutionW / 2.0)
        v = (point_img[1] / (self.pixel_size) - self.roiOffsetH + self.resolutionH / 2.0)

        # ignore points outside the region of interest
        if (u > self.roiResolutionW or u < 0 or v > self.resolutionH or v  < 0): return 0
        return (u, v)

    def rotationMatrix(self, alpha, beta, gamma):
        # standard euler convention zx'z''
        return np.array([[ np.cos(alpha) * np.cos(gamma) - np.sin(alpha) * np.cos(beta) * np.sin(gamma),
                          -np.sin(alpha) * np.cos(gamma) - np.cos(alpha) * np.cos(beta) * np.sin(gamma),
                            np.sin(beta) * np.sin(gamma)],
                         [ np.cos(alpha) * np.sin(gamma) + np.sin(alpha) * np.cos(beta) * np.cos(gamma),
                          -np.sin(alpha) * np.sin(gamma) + np.cos(alpha) * np.cos(beta) * np.cos(gamma),
                           -np.sin(beta) * np.cos(gamma)],
                         [ np.sin(alpha) * np.sin(beta),
                           np.cos(alpha) * np.sin(beta),
                           np.cos(beta)]])

    def endSim(self):
        self.physicalSimulation.endSim()
